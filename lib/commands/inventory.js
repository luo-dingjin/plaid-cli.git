const importLazy = require('import-lazy')(require);

const { contextMiddleware } = importLazy('../cli/context-middleware');
const { fetchAndStoreModules } = importLazy('../environment/inventory');

function inventoryCommand(argv) {
  if (argv.fetch) {
    return fetchAndStoreModules().then(() => { return Promise.resolve(); });
  }

  return Promise.resolve();
}

module.exports = {
  command: 'inventory',
  describe: '管理本地库存缓存，为“plaid工作区”提供模块数据',
  builder: (yargs) => {
    yargs
      .middleware([
        contextMiddleware(),
      ]);
    yargs.option('fetch', {
      describe: 'Fetch module names from Github and store in local cache. Note that there is a rate limit per IP to Github, so you can\'t call this too many times in a short time period.',
      type: 'boolean',
      default: true,
    });
    yargs.example('$0 inventory --fetch', 'Fetch module names from Github and store in local cache.');
    return yargs;
  },
  handler: inventoryCommand,
};
